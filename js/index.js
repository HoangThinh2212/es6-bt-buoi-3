const BASE_URL = "https://635ff9213e8f65f283c08516.mockapi.io";
// thêm biến idEdit để dùng khi chỉnh sửa id
var idEdited = null;

function fetchAllTodo() {
  //render all todos services
  axios({
    url: `${BASE_URL}/toDo`,
    method: "GET",
  })
    //Nếu thành công ~ dùng .then
    .then(function (res) {
      // console.log("res todos: ", res.data);
      renderTodoList(res.data);
    })
    // Nếu thất bại ~ dùng .catch
    .catch(function (err) {
      console.log("err: ", err);
    });
}
// chạy lần đầu khi load trang
fetchAllTodo();

// Add Todos
function addTodo() {
  var data = layThongTinTuForm();

  var newTodo = {
    desc: data.desc,
    isComplete: false, // biến mới khi thêm vào sẽ là chưa hoàn thành
  };
  axios({
    url: `${BASE_URL}/toDo`,
    method: "POST",
    data: newTodo,
  })
    //Nếu thành công ~ dùng .then
    .then(function (res) {
      // thêm vào để clear giá trị sau khi thêm giá trị vào
      document.getElementById("newTask").value = "";

      // console.log("res todos: ", res.data);
      fetchAllTodo();
      console.log("res: ", res);
    })
    // Nếu thất bại ~ dùng .catch
    .catch(function (err) {
      console.log("err: ", err);
    });
}
//Remove all todos
function removeTodo(idTodo) {
  axios({
    url: `${BASE_URL}/toDo/${idTodo}`,
    method: "DELETE",
  })
    .then(function (res) {
      // gọi lại API get all todos
      fetchAllTodo();
      console.log("res: ", res);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

function changeStatus() {
  var statusTodo = document.getElementById("statusToDo");
  let count = 0;
  if (statusTodo.checked) {
    alert("Hello");
  }
}
